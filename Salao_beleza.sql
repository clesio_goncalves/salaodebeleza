SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `Salao_beleza` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `Salao_beleza` ;

-- -----------------------------------------------------
-- Table `Salao_beleza`.`Cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Salao_beleza`.`Cliente` (
  `codigo_cliente` INT NOT NULL AUTO_INCREMENT,
  `nome_cliente` VARCHAR(50) NOT NULL,
  `telefone_cliente` VARCHAR(13) NOT NULL,
  `sexo_cliente` ENUM('M', 'F') NOT NULL,
  `endereco_cliente` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`codigo_cliente`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Salao_beleza`.`Atendimento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Salao_beleza`.`Atendimento` (
  `codigo_atendimento` INT NOT NULL AUTO_INCREMENT,
  `valor_total` FLOAT NOT NULL,
  `data_atendimento` DATE NOT NULL,
  `hora_atendimento` TIME NOT NULL,
  `duracao_atendimento` FLOAT NOT NULL,
  `codigo_cliente` INT NOT NULL,
  PRIMARY KEY (`codigo_atendimento`),
  INDEX `fk_Atendimento_Cliente1_idx` (`codigo_cliente` ASC),
  CONSTRAINT `fk_Atendimento_Cliente1`
    FOREIGN KEY (`codigo_cliente`)
    REFERENCES `Salao_beleza`.`Cliente` (`codigo_cliente`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Salao_beleza`.`Area_corporal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Salao_beleza`.`Area_corporal` (
  `codigo_area_corporal` INT NOT NULL AUTO_INCREMENT COMMENT '		',
  `nome_area_corporal` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`codigo_area_corporal`),
  UNIQUE INDEX `nome_area_corporal_UNIQUE` (`nome_area_corporal` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Salao_beleza`.`Servico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Salao_beleza`.`Servico` (
  `codigo_servico` INT NOT NULL AUTO_INCREMENT,
  `descricao_servico` VARCHAR(100) NOT NULL,
  `valor_servico` FLOAT NOT NULL,
  `duracao_servico` FLOAT NOT NULL,
  `codigo_area_corporal` INT NOT NULL,
  PRIMARY KEY (`codigo_servico`),
  INDEX `fk_Servico_Area_corporal1_idx` (`codigo_area_corporal` ASC),
  CONSTRAINT `fk_Servico_Area_corporal1`
    FOREIGN KEY (`codigo_area_corporal`)
    REFERENCES `Salao_beleza`.`Area_corporal` (`codigo_area_corporal`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Salao_beleza`.`Cargo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Salao_beleza`.`Cargo` (
  `codigo_cargo` INT NOT NULL AUTO_INCREMENT,
  `nome_cargo` VARCHAR(50) NOT NULL,
  `salario` FLOAT NOT NULL,
  `qnt_horas_semana` INT NOT NULL,
  PRIMARY KEY (`codigo_cargo`),
  UNIQUE INDEX `nome_cargo_UNIQUE` (`nome_cargo` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Salao_beleza`.`Funcionario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Salao_beleza`.`Funcionario` (
  `codigo_funcionario` INT NOT NULL AUTO_INCREMENT COMMENT '		',
  `nome_funcionario` VARCHAR(50) NOT NULL,
  `telefone_funcionario` VARCHAR(13) NOT NULL,
  `sexo_funcionario` ENUM('M', 'F') NOT NULL,
  `endereco_funcionario` VARCHAR(100) NOT NULL,
  `codigo_cargo` INT NOT NULL,
  PRIMARY KEY (`codigo_funcionario`),
  INDEX `fk_Funcionario_Cargo1_idx` (`codigo_cargo` ASC),
  CONSTRAINT `fk_Funcionario_Cargo1`
    FOREIGN KEY (`codigo_cargo`)
    REFERENCES `Salao_beleza`.`Cargo` (`codigo_cargo`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Salao_beleza`.`Compra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Salao_beleza`.`Compra` (
  `codigo_compra` INT NOT NULL AUTO_INCREMENT,
  `valor_total` FLOAT NOT NULL,
  `data_compra` DATE NOT NULL,
  `codigo_cliente` INT NOT NULL,
  PRIMARY KEY (`codigo_compra`),
  INDEX `fk_Compra_Cliente1_idx` (`codigo_cliente` ASC),
  CONSTRAINT `fk_Compra_Cliente1`
    FOREIGN KEY (`codigo_cliente`)
    REFERENCES `Salao_beleza`.`Cliente` (`codigo_cliente`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Salao_beleza`.`Produto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Salao_beleza`.`Produto` (
  `codigo_produto` INT NOT NULL AUTO_INCREMENT,
  `nome_produto` VARCHAR(50) NOT NULL,
  `valor_unitario` FLOAT NULL,
  PRIMARY KEY (`codigo_produto`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Salao_beleza`.`Funcionario_Servico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Salao_beleza`.`Funcionario_Servico` (
  `codigo_funcionario` INT NOT NULL,
  `codigo_servico` INT NOT NULL,
  `horario_inicio` TIME NOT NULL,
  `horario_fim` TIME NOT NULL,
  PRIMARY KEY (`codigo_funcionario`, `codigo_servico`),
  INDEX `fk_Funcionario_has_Servico_Servico1_idx` (`codigo_servico` ASC),
  INDEX `fk_Funcionario_has_Servico_Funcionario_idx` (`codigo_funcionario` ASC),
  CONSTRAINT `fk_Funcionario_has_Servico_Funcionario`
    FOREIGN KEY (`codigo_funcionario`)
    REFERENCES `Salao_beleza`.`Funcionario` (`codigo_funcionario`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Funcionario_has_Servico_Servico1`
    FOREIGN KEY (`codigo_servico`)
    REFERENCES `Salao_beleza`.`Servico` (`codigo_servico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Salao_beleza`.`Atendimento_Servico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Salao_beleza`.`Atendimento_Servico` (
  `codigo_atendimento` INT NULL,
  `codigo_servico` INT NOT NULL,
  PRIMARY KEY (`codigo_atendimento`, `codigo_servico`),
  INDEX `fk_Atendimento_has_Servico_Servico1_idx` (`codigo_servico` ASC),
  INDEX `fk_Atendimento_has_Servico_Atendimento1_idx` (`codigo_atendimento` ASC),
  CONSTRAINT `fk_Atendimento_has_Servico_Atendimento1`
    FOREIGN KEY (`codigo_atendimento`)
    REFERENCES `Salao_beleza`.`Atendimento` (`codigo_atendimento`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Atendimento_has_Servico_Servico1`
    FOREIGN KEY (`codigo_servico`)
    REFERENCES `Salao_beleza`.`Servico` (`codigo_servico`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Salao_beleza`.`Item_produto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Salao_beleza`.`Item_produto` (
  `codigo_compra` INT NULL,
  `codigo_produto` INT NOT NULL,
  `quantidade` INT NOT NULL,
  `valor_item` FLOAT NOT NULL,
  PRIMARY KEY (`codigo_compra`, `codigo_produto`),
  INDEX `fk_Compra_has_Produto_Produto1_idx` (`codigo_produto` ASC),
  INDEX `fk_Compra_has_Produto_Compra1_idx` (`codigo_compra` ASC),
  CONSTRAINT `fk_Compra_has_Produto_Compra1`
    FOREIGN KEY (`codigo_compra`)
    REFERENCES `Salao_beleza`.`Compra` (`codigo_compra`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Compra_has_Produto_Produto1`
    FOREIGN KEY (`codigo_produto`)
    REFERENCES `Salao_beleza`.`Produto` (`codigo_produto`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
