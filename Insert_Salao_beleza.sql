-- Cliente

insert into Cliente values (default, 'Alilia Maiara', '9999-9999', 'F', 'Junco - Picos-PI');

insert into Cliente values (default, 'Williane', '9999-9999', 'F', 'Cohab - Picos-PI');

insert into Cliente values (default, 'Isaine', '9999-9999', 'F', 'Junco - Picos-PI');

-- AreaCorporal

insert into Area_corporal values (default, 'Mão');

insert into Area_corporal values (default, 'Pé');

insert into Area_corporal values (default, 'Cabelo');

-- Serviço

insert into Servico values (default, 'Lavar o cabelo', 20.0, 2.5, 3);

insert into Servico values (default, 'Cortar as unhas das mãos', 30.0, 1.5, 1);

insert into Servico values (default, 'Cortar as unhas dos pés', 30.0, 2.0, 2);

-- Atendimento
insert into Atendimento values (default, 0.0, '2014-09-09', '12:40', 0.0, 1);

insert into Atendimento values (default, 0.0, '2014-09-09', '15:40', 0.0, 2);

-- Atendimento_servico
insert into Atendimento_Servico values (1, 2);
insert into Atendimento_Servico values (1, 1);
insert into Atendimento_Servico values (2, 3);

-- Cargo
insert into Cargo values (default, 'cabelereiro', 100.0, 8);
insert into Cargo values (default, 'manicure', 200.0, 8);
insert into Cargo values (default, 'pedicure', 400.0, 6);

-- Funcionario
insert into Funcionario values (default, 'Maria', '9987-7568', 'F', 'Junco - Picos-PI', 2);
insert into Funcionario values (default, 'José', '9912-3456', 'M', 'Junco - Picos-PI', 1);
insert into Funcionario values (default, 'Josefa', '9878-2456', 'F', 'Junco - Picos-PI', 3);


-- Funcionario_servico
insert into Funcionario_Servico values (2, 1, '00:00', '00:00');
insert into Funcionario_Servico values (1, 2, '00:00', '00:00');
insert into Funcionario_Servico values (3, 3, '00:00', '00:00');

-- Produto
insert into Produto values(default, 'Creme de cabelo', 56.0);
insert into Produto values(default, 'Shampoo', 26.0);

-- Compra
insert into Compra values (default, 0.0, '09-09-2014', 1);

-- ItemProduto
insert into Item_produto values (1, 2, 2, 52.0);
insert into Item_produto values (1, 1, 2, 112.0);


-- Consultas

-- Selecionar todos os serviços ofertados pelo salão
select *
from Servico;

-- Saber o cargo de cada funcionario do salão
select f.nome_funcionario, c.nome_cargo
from Funcionario as f 
inner join Cargo as c 
on f.codigo_cargo = c.codigo_cargo;

-- Duração e valor de cada seviço ordenado por nome
select descricao_servico, valor_servico, duracao_servico
from Servico
order by descricao_servico;

-- Relação de todos os produtos ordenados pelo menor valor
select * 
from Produto
order by valor_unitario;

-- Relação de todos os clientes que compraram em setembro de 2014
select c.nome_cliente, co.data_compra
from Cliente as c
inner join Compra as co
on co.codigo_cliente = c.codigo_cliente
where co.data_compra between '2014-09-01' and '2014-09-30';
